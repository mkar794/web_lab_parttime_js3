"use strict";



// TODO Create object prototypes here
function album(title, artist, year, genre, tracks) {
    this.title = title;
    this.artist = artist;
    this.year = year;
    this.genre = genre;
    this.tracks = tracks;
    this.toString = function() {
        return ("Album: " +"'"+ this.title +"'" + ", " + "released in" + " " + this.year + " " + "by " + this.artist);
    }
}

var taylor = new album("1989", "Taylor Swift", "2014", "Pop", 
    [ "Welcome to New York", "Blank Space", "Style", "Out of the Woods", "All You Had to Do Was Stay", "Shake It Off", 
        "I Wish You Would", "Bad Blood", "Wildest Dreams", "How You Get the Girl", "This Love", "I Know Places", "Clean" ]
    );

var pentatonix = new album("PTX Vol. IV - Classics", "Pentatonix", "2017", "Classics", 
    ["Bohemian Rhapsody", "Imagine", "Boogie Woogie Bugle Boy", "Over the Rainbow", "Take On Me", "Can't Help Falling In Love"]
    );

 var BryanAdams = new album("18 Til I Die", "Bryan Adams", "1996", "Soft Rock", 
    ["Have You Ever Really Loved a Woman?", "The Only Thing That Looks Good on Me Is You", "Let's Make a Night to Remember", 
    "I'll Always Be Right There", "Star", "18 till I die"]
    );

// TODO Create functions here
console.log("test1");

function getAlbums(newAlbum, newAlbum1, newAlbum2) {
    return [newAlbum, newAlbum1, newAlbum2];
}

var albumArray = getAlbums(taylor, pentatonix, BryanAdams);

function printAlbums(albumObj) {
    for(var i = 0; i < albumObj.length; i++) {
        //console.log("test1");
        console.log(albumObj[i].toString());
        //console.log("testbreak")
        for(var j = 0; j < albumObj[i].tracks.length; j++) {
            console.log(albumObj[i].tracks[j]);
        }
        
    }
}
console.log("test2");
printAlbums(albumArray);


// TODO Complete the program here


